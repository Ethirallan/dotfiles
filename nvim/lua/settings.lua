local o = vim.o
local wo = vim.wo
local bo = vim.bo
local cmd = vim.cmd

cmd 'syntax enable'

-- global options
o.hlsearch = true
o.incsearch = true
o.ignorecase = true
o.scrolloff = 10
o.mouse = 'a'
o.errorbells = false
o.smartcase = true
o.undodir = '/Users/ethirallan/.config/nvim/undodir'
o.clipboard = 'unnamed,unnamedplus'
o.splitbelow = true
o.splitright = true
o.hidden = true
o.updatetime = 300
o.shortmess = 'c'
-- o.smarttab = true
-- open issue about bo.
o.tabstop = 2
o.softtabstop = 2
o.shiftwidth = 2
o.expandtab = true
o.smartindent = true
o.swapfile = false
o.undofile = true

-- window-local options
wo.number = true
wo.wrap = false
wo.relativenumber = true
-- wo.signcolumn = 'number'

-- buffer-local options
bo.tabstop = 2
bo.softtabstop = 2
bo.shiftwidth = 2
bo.expandtab = true
bo.smartindent = true
bo.swapfile = false
bo.undofile = true
