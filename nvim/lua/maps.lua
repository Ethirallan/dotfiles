local map = vim.api.nvim_set_keymap

-- map the leader key
map('n', '<Space>', '', {})
vim.g.mapleader = ' '  -- 'vim.g' sets global variables

local options = { noremap = true }

map('n', '<leader><esc>', ':nohlsearch<cr>', options)
map('n', '<leader>n', ':bnext<cr>', options)
map('n', '<leader>p', ':bprev<cr>', options)

-- best remap ever
map('v', 'p', '"_dP', options)
map('n', '<leader>d', '"_d', options)

map('i', '<C-n>', '<CR>', {})
map('c', '<C-n>', '<CR>', {})
map('i', '<C-j>', '<C-n>', options)
map('c', '<C-j>', '<C-n>', options)

-- Vi panel movement
map('', '<C-left>', '<C-w>h', options)
map('', '<C-down>', '<C-w>j', options)
map('', '<C-up>', '<C-w>k', options)
map('', '<C-right>', '<C-w>l', options)
map('', '<S-up>', ':resize -5<CR>', options)
map('', '<S-down>', ':resize +5<CR>', options)
map('', '<S-left>', ':vertical resize -5<CR>', options)
map('', '<S-right>', ':vertical resize +5<CR>', options)
