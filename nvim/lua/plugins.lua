return require("packer").startup(function()
  -- Packer can manage itself as an optional plugin
  use({ "wbthomason/packer.nvim", opt = true })

  -- Color scheme
  use({ "ellisonleao/gruvbox.nvim", requires = { "rktjmp/lush.nvim" } })
  -- use 'rafamadriz/gruvbox'
  use({ "glepnir/dashboard-nvim" })
  use({
    "nvim-lualine/lualine.nvim",
    requires = { "kyazdani42/nvim-web-devicons", opt = true },
  })

  -- The Telescope
  use({
    "nvim-telescope/telescope.nvim",
    requires = { { "nvim-lua/popup.nvim" }, { "nvim-lua/plenary.nvim" } },
  })

  use({ "nvim-telescope/telescope-file-browser.nvim" })

  -- use {
  --   'ahmedkhalf/project.nvim',
  --   config = function()
  --     require('project_nvim').setup {
  --       -- your configuration comes here
  --       -- or leave it empty to use the default settings
  --       -- refer to the configuration section below
  --     }
  --   end
  -- }

  use({
    "folke/trouble.nvim",
    requires = "kyazdani42/nvim-web-devicons",
    config = function()
      require("trouble").setup({
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      })
    end,
  })

  -- Notes
  use({
    "nvim-neorg/neorg",
    tag = "*",
    run = ":Neorg sync-parsers",
    requires = {
      "nvim-lua/plenary.nvim",
      "nvim-neorg/neorg-telescope",
    }, -- Be sure to pull in the repo
  })

  -- Git
  use({ "tpope/vim-fugitive" })
  -- use { 'airblade/vim-gitgutter' }
  use({
    "lewis6991/gitsigns.nvim",
    requires = {
      "nvim-lua/plenary.nvim",
    },
  })
  use({ "sindrets/diffview.nvim" })

  -- Tmux
  use({ "christoomey/vim-tmux-navigator" })
  use({ "tmux-plugins/vim-tmux-focus-events" })
  use({ "roxma/vim-tmux-clipboard" })

  -- Utils
  use({ "windwp/nvim-autopairs" })
  -- use { 'tpope/vim-commentary' }
  use({
    "numToStr/Comment.nvim",
    config = function()
      require("Comment").setup()
    end,
  })
  use({ "lukas-reineke/indent-blankline.nvim" })

  use({ "akinsho/nvim-toggleterm.lua" })

  use({ "dbeniamine/cheat.sh-vim" })

  -- use { 'jamestthompson3/nvim-remote-containers' }
  -- use { 'https://codeberg.org/esensar/nvim-dev-container' }


  -- LSP
  use({
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
    "neovim/nvim-lspconfig",
    "jose-elias-alvarez/null-ls.nvim",
    "jay-babu/mason-null-ls.nvim",
    "WhoIsSethDaniel/mason-tool-installer.nvim",
  })
  -- use({ "https://github.com/github/copilot.vim" })
  use({ "zbirenbaum/copilot.lua" })
  use {
    "zbirenbaum/copilot-cmp",
    after = { "copilot.lua" },
    config = function()
      require("copilot_cmp").setup({
        formatters = {
          label = require("copilot_cmp.format").format_label_text,
          -- insert_text = require("copilot_cmp.format").format_insert_text,
          insert_text = require("copilot_cmp.format").remove_existing,
          preview = require("copilot_cmp.format").deindent,
        },
      })
    end
  }

  use({ "hrsh7th/nvim-cmp" })
  use({ "hrsh7th/cmp-nvim-lsp" })
  use({ "hrsh7th/cmp-nvim-lua" })
  use({ "hrsh7th/cmp-path" })
  use({ "hrsh7th/cmp-buffer" })
  use({ "hrsh7th/cmp-calc" })
  use({ "f3fora/cmp-spell" })


  use({ "glepnir/lspsaga.nvim", branch = "main" })
  use({ "ray-x/lsp_signature.nvim" })
  use({ "monaqa/dial.nvim" })

  -- Debugging
  use({ "mfussenegger/nvim-dap" })
  use({ "rcarriga/nvim-dap-ui" })
  use({ "theHamsta/nvim-dap-virtual-text" })
  use({ "nvim-telescope/telescope-dap.nvim" })
  use({ "leoluz/nvim-dap-go" })
  use({ "mfussenegger/nvim-dap-python" })
  use({ "folke/neodev.nvim" })

  -- Better quickfix
  use({ "kevinhwang91/nvim-bqf" })
  use({ "mhinz/vim-grepper" })

  -- Treesitter
  -- Post-install/update hook with neovim command
  use({ "nvim-treesitter/nvim-treesitter", commit = "33eb472b459f1d2bf49e16154726743ab3ca1c6d", run = ":TSUpdate" })
  -- use({ "nvim-treesitter/nvim-treesitter-context" })
  use({ "JoosepAlviste/nvim-ts-context-commentstring" })
  use({ "windwp/nvim-ts-autotag" })

  -- Snippets
  use({ "hrsh7th/vim-vsnip" })
  use({ "hrsh7th/vim-vsnip-integ" })
  use({ "hrsh7th/cmp-vsnip" })
  use({ "Neevash/awesome-flutter-snippets" })
  use({ "onsails/lspkind.nvim" })

  -- Tree
  use({ "kyazdani42/nvim-web-devicons" }) -- for file icons
  use({ "kyazdani42/nvim-tree.lua" })

  -- Dart && Flutter
  use({ "akinsho/flutter-tools.nvim" })
  use({ "dart-lang/dart-vim-plugin" })

  -- other
  use({
    "giusgad/pets.nvim",
    requires = {
      "giusgad/hologram.nvim",
      "MunifTanjim/nui.nvim",
    }
  })
  use({
    "tamton-aquib/duck.nvim",
    config = function()
      vim.keymap.set("n", "<leader>dd", function()
        require("duck").hatch("🐤")
      end, {})
      vim.keymap.set("n", "<leader>dk", function()
        require("duck").cook()
      end, {})
    end,
  })
end)
