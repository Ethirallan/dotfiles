local map = vim.api.nvim_set_keymap
local options = { noremap = true }

map('n', '<leader>no', ':Neorg workspace notes<cr>', options)
map('n', '<leader>ne', ':Neorg return<cr>', options)

require('neorg').setup {
    -- Tell Neorg what modules to load
    load = {
        ["core.defaults"] = {}, -- Load all the default modules
        ["core.export"] = {},
        ["core.concealer"] = {
            config = {
                folds = false
            }
        }, -- Allows for use of icons
        ["core.dirman"] = { -- Manage your directories with Neorg
            config = {
                workspaces = {
                    notes = "~/neorg/notes",
                    sp = "~/neorg/sp",
                    work = "~/neorg/work"
                },
                -- Automatically detect whenever we have entered a subdirectory of a workspace
                autodetect = true,
                -- Automatically change the directory to the root of the workspace every time
                -- autochdir = true,
            }
        },
        ["core.integrations.telescope"] = {}, -- Enable the telescope module
        ["core.completion"] = {
          config = {
            engine = "nvim-cmp" -- We current support nvim-compe and nvim-cmp only
          }
        }
    },
}

-- This sets the leader for all Neorg keybinds. It is separate from the regular <Leader>,
-- And allows you to shove every Neorg keybind under one "umbrella".
local neorg_leader = "<Leader>o" -- You may also want to set this to <Leader>o for "organization"

-- Require the user callbacks module, which allows us to tap into the core of Neorg
local neorg_callbacks = require('neorg.callbacks')

-- Listen for the enable_keybinds event, which signals a "ready" state meaning we can bind keys.
-- This hook will be called several times, e.g. whenever the Neorg Mode changes or an event that
-- needs to reevaluate all the bound keys is invoked
neorg_callbacks.on_event("core.keybinds.events.enable_keybinds", function(_, keybinds)

	-- Map all the below keybinds only when the "norg" mode is active
	keybinds.map_event_to_mode("norg", {
		n = { -- Bind keys in normal mode
        -- Keys for managing TODO items and setting their states
        { "gtu", "core.norg.qol.todo_items.todo.task_undone" },
        { "gtp", "core.norg.qol.todo_items.todo.task_pending" },
        { "gtd", "core.norg.qol.todo_items.todo.task_done" },
        { "gth", "core.norg.qol.todo_items.todo.task_on_hold" },
        { "gtc", "core.norg.qol.todo_items.todo.task_cancelled" },
        { "gtr", "core.norg.qol.todo_items.todo.task_recurring" },
        { "gti", "core.norg.qol.todo_items.todo.task_important" },
        { "<C-Space>", "core.norg.qol.todo_items.todo.task_cycle" },

        -- Keys for managing GTD
        { neorg_leader .. "tc", "core.gtd.base.capture" },
        { neorg_leader .. "tv", "core.gtd.base.views" },
        { neorg_leader .. "te", "core.gtd.base.edit" },

        -- Keys for managing notes
        { neorg_leader .. "nn", "core.norg.dirman.new.note" },

        { "<CR>", "core.norg.esupports.hop.hop-link" },
        { "<C-no>", "core.norg.esupports.hop.hop-link", "vsplit" },

        { "<M-k>", "core.norg.manoeuvre.item_up" },
        { "<M-j>", "core.norg.manoeuvre.item_down" },
    },

    o = {
        { "ah", "core.norg.manoeuvre.textobject.around-heading" },
        { "ih", "core.norg.manoeuvre.textobject.inner-heading" },
        { "at", "core.norg.manoeuvre.textobject.around-tag" },
        { "it", "core.norg.manoeuvre.textobject.inner-tag" },
        { "al", "core.norg.manoeuvre.textobject.around-whole-list" },
    },

    i = { -- Bind in insert mode
      { "<C-l>", "core.integrations.telescope.insert_link" },
    },
	}, { silent = true, noremap = true })

end)
