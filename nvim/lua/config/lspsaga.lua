local map = vim.api.nvim_set_keymap

require("lspsaga").setup({
  code_action = {
    keys = {
      quit = { "q", "<ESC>" },
      exec = "<CR>",
    },
  },
  code_action_prompt = { enable = false, sign = true, sign_priority = 40, virtual_text = true },

  ui = {
    -- theme = "round",
    --
    -- border = "solid",
    winblend = 0,
    expand = "",
    collaspe = "",
    preview = " ",
    code_action = "💡",
    diagnostic = "🐞",
    incoming = " ",
    outgoing = " ",
    colors = {
      --float window normal bakcground color
      normal_bg = "#1d1536",
      --title background color
      title_bg = "#afd700",
      red = "#e95678",
      magenta = "#b33076",
      orange = "#FF8700",
      yellow = "#f7bb3b",
      green = "#afd700",
      cyan = "#36d0e0",
      -- blue = "#61afef",
      blue = "#fafafa",
      purple = "#CBA6F7",
      white = "#d1d4cf",
      black = "#1c1c19",
    },
    kind = {},
  },
})

local options = { noremap = true, silent = true }
map("n", "<leader>gh", ":Lspsaga lsp_finder<CR>", options)
map("n", "<leader>a", ":Lspsaga code_action<CR>", options)
map("v", "<leader>a", ":<C-U>Lspsaga range_code_action<CR>", options)

map("n", "<leader>K", ":Lspsaga hover_doc<CR>", options)
map("n", "<leader>o", ":Lspsaga outline<CR>", options)
-- map('n', '<C-f>', "<cmd>lua require('lspsaga.action').smart_scroll_with_saga(1)<CR>", options) -- scroll down
-- map('n', '<C-d>', "<cmd>lua require('lspsaga.action').smart_scroll_with_saga(-1)<CR>", options) -- scroll up

map("n", "<leader>gs", ":Lspsaga signature_help<CR>", options)
map("n", "<leader>Rn", ":Lspsaga rename<CR>", options)
map("n", "<leader>gd", ":Lspsaga preview_definition<CR>", options)

-- float terminal also you can pass the cli command in open_float_terminal function
-- nnoremap <silent> <A-d> <cmd>lua require('lspsaga.floaterm').open_float_terminal()<CR> -- or open_float_terminal('lazygit')<CR>
-- tnoremap <silent> <A-d> <C-\><C-n>:lua require('lspsaga.floaterm').close_float_terminal()<CR>
