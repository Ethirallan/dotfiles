local map = vim.api.nvim_set_keymap

local options = { noremap = true, silent = true }

map('n', '<leader>cs', "<cmd>call cheat#cheat('', getcurpos()[1], getcurpos()[1], 0, 0, '!')<cr>", options)
