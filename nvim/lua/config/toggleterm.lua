local bufmap = vim.api.nvim_buf_set_keymap
local map = vim.api.nvim_set_keymap

require("toggleterm").setup{
 open_mapping = [[<leader>tn]],
 insert_mappings = false,
 start_in_insert = false,
}

local options = { noremap = true }
-- map('n', '<leader>tt', '<cmd>ToggleTerm<cr>', options)
map('n', '<leader>to', '<cmd>ToggleTermOpenAll<cr>', options)
map('n', '<leader>tc', '<cmd>ToggleTermCloseAll<cr>', options)


function _G.set_terminal_keymaps()
  bufmap(0, 't', '<esc>', [[<C-\><C-n>]], options)
  -- bufmap(0, 't', 'ne', [[<C-\><C-n>]], options)
  bufmap(0, 't', '<C-y>', [[<C-\><C-n><C-W>h]], options)
  bufmap(0, 't', '<C-n>', [[<C-\><C-n><C-W>j]], options)
  bufmap(0, 't', '<C-e>', [[<C-\><C-n><C-W>k]], options)
  bufmap(0, 't', '<C-o>', [[<C-\><C-n><C-W>l]], options)
end

-- if you only want these mappings for toggle term use term://*toggleterm#* instead
vim.cmd('autocmd! TermOpen term://* lua set_terminal_keymaps()')
