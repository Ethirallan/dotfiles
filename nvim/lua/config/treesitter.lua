local map = vim.api.nvim_set_keymap
-- local parser_configs = require("nvim-treesitter.parsers").get_parser_configs()
-- parser_configs.norg = {
--     install_info = {
--         url = "https://github.com/nvim-neorg/tree-sitter-norg",
--         files = { "src/parser.c", "src/scanner.cc" },
--         branch = "main"
--     },
-- }
--
-- parser_configs.norg_meta = {
--     install_info = {
--         url = "https://github.com/nvim-neorg/tree-sitter-norg-meta",
--         files = { "src/parser.c" },
--         branch = "main"
--     },
-- }
--
-- parser_configs.norg_table = {
--     install_info = {
--         url = "https://github.com/nvim-neorg/tree-sitter-norg-table",
--         files = { "src/parser.c" },
--         branch = "main"
--     },
-- }

-- local parser = require("nvim-treesitter.parsers").get_parser_configs()
-- parser.dart = {
--   install_info = {
--     url = "https://github.com/UserNobody14/tree-sitter-dart",
--     files = { "src/parser.c", "src/scanner.c" },
--     revision = "8aa8ab977647da2d4dcfb8c4726341bee26fbce4", -- The last commit before the snail speed
--   },
-- }

require("nvim-treesitter.configs").setup({
  ensure_installed = { "python", "dart", "typescript", "vue", "html", "css", "norg", "tsx" }, -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  highlight = {
    enable = true, -- false will disable the whole extension
  },
  indent = {
    enable = true, -- treesitter indentation
  },
  context_commentstring = {
    enable = true,
    config = {
      -- vue = {
      --   style_element = '// %s',
      -- },
    },
  },
  autotag = {
    enable = true,
  },
})

-- require("treesitter-context").setup({
--   enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
--   max_lines = 0, -- How many lines the window should span. Values <= 0 mean no limit.
--   min_window_height = 0, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
--   line_numbers = true,
--   multiline_threshold = 20, -- Maximum number of lines to collapse for a single context line
--   trim_scope = "outer", -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
--   mode = "cursor", -- Line used to calculate context. Choices: 'cursor', 'topline'
--   -- Separator between context and content. Should be a single character string, like '-'.
--   -- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
--   separator = nil,
--   zindex = 20, -- The Z-index of the context window
-- })
-- map("n", "<leader>tsc", ":TSContextToggle<cr>", { noremap = true, silent = true })
