local lspconfig = require("lspconfig")
local null_ls = require("null-ls")
local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
local map = vim.api.nvim_set_keymap

require("mason").setup()
require("mason-lspconfig").setup()

local on_attach = function(client, bufnr)
  require("lsp_signature").on_attach()

  local function buf_set_keymap(...)
    vim.api.nvim_buf_set_keymap(bufnr, ...)
  end

  -- Mappings
  local opts = { noremap = true, silent = true }
  buf_set_keymap("n", "ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
  buf_set_keymap("n", "gD", "<Cmd>lua vim.lsp.buf.declaration()<CR>", opts)
  buf_set_keymap("n", "gd", "<Cmd>lua vim.lsp.buf.definition()<CR>", opts)
  buf_set_keymap("n", "K", "<Cmd>lua vim.lsp.buf.hover()<CR>", opts)
  buf_set_keymap("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
  buf_set_keymap("n", "gs", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
  buf_set_keymap("n", "gR", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
  buf_set_keymap("n", "[d", "<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>", opts)
  buf_set_keymap("n", "]d", "<cmd>lua vim.lsp.diagnostic.goto_next()<CR>", opts)
  buf_set_keymap("n", "<leader>rn", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)
  buf_set_keymap("n", "E", "<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>", opts)

  -- Set some keybinds conditional on server capabilities
  if client.server_capabilities.documentFormattingProvider then
    buf_set_keymap("n", "F", "<cmd>lua vim.lsp.buf.format()<CR>", opts)
  elseif client.server_capabilities.documentRangeFormattingProvider then
    buf_set_keymap("n", "F", "<cmd>lua vim.lsp.buf.range_formatting()<CR>", opts)
  end
end

require("mason-null-ls").setup({
  ensure_installed = {
    "black",
    "flake8",
  },
  automatic_installation = false,
  automatic_setup = true,
})

null_ls.setup({
  -- on_attach = function(client, bufnr)
  --     if client.supports_method("textDocument/formatting") then
  --         vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
  --         vim.api.nvim_create_autocmd("BufWritePre", {
  --             group = augroup,
  --             buffer = bufnr,
  --             callback = function()
  --                 -- on 0.8, you should use vim.lsp.buf.format({ bufnr = bufnr }) instead
  --                 vim.lsp.buf.format()
  --             end,
  --         })
  --     end
  -- end,
  on_attach = on_attach,
  sources = {
    null_ls.builtins.formatting.black.with({
      extra_args = { "--line-length=120" },
    }),
    null_ls.builtins.diagnostics.flake8.with({
      extra_args = { "--ignore=W504,E402,E501,E203,W503", "--max-line-length=120" },
    }),
  },
})

-- require("mason-null-ls").setup_handlers() -- If `automatic_setup` is true.

require("mason-lspconfig").setup_handlers({
  -- The first entry (without a key) will be the default handler
  -- and will be called for each installed server that doesn't have
  -- a dedicated handler.
  function(server_name) -- default handler (optional)
    lspconfig[server_name].setup({
      on_attach = on_attach,
    })
  end,
  -- Next, you can provide a dedicated handler for specific servers.
  -- For example, a handler override for the `rust_analyzer`:
  ["pyright"] = function()
    require("lspconfig")["pyright"].setup({
      root_dir = lspconfig.util.root_pattern(".git"),
      on_attach = on_attach,
      settings = {
        python = {
          analysis = {
            typeCheckingMode = "basic",
          },
        },
      },
    })
  end,
})

require("mason-tool-installer").setup({

  -- a list of all tools you want to ensure are installed upon
  -- start; they should be the names Mason uses for each tool
  ensure_installed = {
    "lua-language-server",
    "vim-language-server",
    "stylua",
    "pyright",
    "flake8",
    "black",
    "svelte-language-server",
    "typescript-language-server",
    "tailwindcss-language-server",
    "dockerfile-language-server",
    "css-lsp",
    "emmet-ls",
    "html-lsp",
  },

  -- if set to true this will check each tool for updates. If updates
  -- are available the tool will be updated. This setting does not
  -- affect :MasonToolsUpdate or :MasonToolsInstall.
  -- Default: false
  auto_update = false,

  -- automatically install / update on startup. If set to false nothing
  -- will happen on startup. You can use :MasonToolsInstall or
  -- :MasonToolsUpdate to install tools and check for updates.
  -- Default: true
  run_on_start = true,

  -- set a delay (in ms) before the installation starts. This is only
  -- effective if run_on_start is set to true.
  -- e.g.: 5000 = 5 second delay, 10000 = 10 second delay, etc...
  -- Default: 0
  start_delay = 3000, -- 3 second delay

  -- Only attempt to install if 'debounce_hours' number of hours has
  -- elapsed since the last time Neovim was started. This stores a
  -- timestamp in a file named stdpath('data')/mason-tool-installer-debounce.
  -- This is only relevant when you are using 'run_on_start'. It has no
  -- effect when running manually via ':MasonToolsInstall' etc....
  -- Default: nil
  debounce_hours = 5, -- at least 5 hours between attempts to install/update
})

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)
local optionsFlutter = { noremap = true, silent = true }

map('n', '<leader>fr', ':FlutterReload<cr>', optionsFlutter)
map('n', '<leader>fR', ':FlutterRun<cr>', optionsFlutter)
map('n', '<leader>fo', ':FlutterOutline<cr>', optionsFlutter)
map('n', '<leader>ft', ':FlutterDevTools<cr>', optionsFlutter)
require("flutter-tools").setup({
  dev_log = {
    open_cmd = "tabedit", -- command to use to open the log buffer
  },
  lsp = {
    -- cmd = { "dart", "/Users/ethirallan/Development/flutter/bin/cache/dart-sdk/bin/snapshots/analysis_server.dart.snapshot", "--lsp" },
    on_attach = on_attach,
    capabilities = capabilities,
  },
})
