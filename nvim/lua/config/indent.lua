require("ibl").setup {
  -- buftype_exclude = {"terminal", "dashboard", "nofile"},
  -- filetype_exclude = {"dashboard", "terminal", "git", "octo"},
  -- show_current_context = true,
  -- use_treesitter = true,
  -- show_end_of_line = true
  indent = {
    char = "▏",
  },
  scope = {
    enabled = false,
    show_start = true,
    show_end = false,
    exclude = {
      language = {
        "help"
      }
    },
  },
}
