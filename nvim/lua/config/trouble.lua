local map = vim.api.nvim_set_keymap
-- local actions = require("telescope.actions")

local options = { noremap = true, silent = true }
map('n', '<leader>xx', '<cmd>Trouble<cr>', options)
map('n', '<leader>xw', '<cmd>Trouble lsp_workspace_diagnostics<cr>', options)
map('n', '<leader>xd', '<cmd>Trouble lsp_document_diagnostics<cr>', options)
map('n', '<leader>xl', '<cmd>Trouble loclist<cr>', options)
map('n', '<leader>xq', '<cmd>Trouble quickfix<cr>', options)
map('n', 'gr', '<cmd>Trouble lsp_references<cr>', options)

-- jump to the next item, skipping the groups
-- require("trouble").next({skip_groups = true, jump = true});

-- jump to the previous item, skipping the groups
-- require("trouble").previous({skip_groups = true, jump = true});
