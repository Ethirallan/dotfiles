local map = vim.api.nvim_set_keymap

local options = { noremap = true }

map('n', '<leader>gD', ':Gvdiff<cr>', options)
map('n', 'gdm', ':diffget //2<cr>', options)
map('n', 'gdo', ':diffget //3<cr>', options)

vim.opt.fillchars:append { diff = "╱" }
