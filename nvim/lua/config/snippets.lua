local map = vim.api.nvim_set_keymap

local t = function(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

local check_back_space = function()
    local col = vim.fn.col('.') - 1
    if col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
        return true
    else
        return false
    end
end

-- Use (s-)tab to:
--- move to prev/next item in completion menuone
--- jump to prev/next snippet's placeholder
_G.tab_complete = function()
  if vim.fn.pumvisible() == 1 then
    return t "<C-n>"
  elseif vim.fn.call("vsnip#available", {1}) == 1 then
    return t "<Plug>(vsnip-expand-or-jump)"
  elseif check_back_space() then
    return t "<Tab>"
  else
    return vim.fn['compe#complete']()
  end
end
_G.s_tab_complete = function()
  if vim.fn.pumvisible() == 1 then
    return t "<C-p>"
  elseif vim.fn.call("vsnip#jumpable", {-1}) == 1 then
    return t "<Plug>(vsnip-jump-prev)"
  else
    return t "<S-Tab>"
  end
end

local options = { expr = true }

-- Expand
map('i', '<C-j>', "vsnip#expandable()    ? '<Plug>(vsnip-expand)'         : '<C-j>'", options)
map('s', '<C-j>', "vsnip#expandable()    ? '<Plug>(vsnip-expand)'         : '<C-j>'", options)

-- Expand or jump
map('i', '<C-l>', "vsnip#available(1)    ? '<Plug>(vsnip-expand-or-jump)' : '<C-l>'", options)
map('s', '<C-l>', "vsnip#available(1)    ? '<Plug>(vsnip-expand-or-jump)' : '<C-l>'", options)

-- Jump forward or backward
map("i", "<Tab>", "v:lua.tab_complete()", options)
map("s", "<Tab>", "v:lua.tab_complete()", options)
map("i", "<S-Tab>", "v:lua.s_tab_complete()", options)
map("s", "<S-Tab>", "v:lua.s_tab_complete()", options)
-- map('i', '<Tab>', "vsnip#jumpable(1)     ? '<Plug>(vsnip-jump-next)'      : '<Tab>'", options)
-- map('s', '<Tab>', "vsnip#jumpable(1)     ? '<Plug>(vsnip-jump-next)'      : '<Tab>'", options)
-- map('i', '<S-Tab>', "vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'", options)
-- map('s', '<S-Tab>', "vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'", options)
