local map = vim.api.nvim_set_keymap
local dap = require("dap")
local dapui = require("dapui")

map("n", "<leader>dct", '<cmd>lua require"dap".continue()<CR>', {})
map("n", "<leader>dtg", '<cmd>lua require"dap-go".debug_test()<CR>', {})
map("n", "<leader>dsv", '<cmd>lua require"dap".step_over()<CR>', {})
map("n", "<leader>dsi", '<cmd>lua require"dap".step_into()<CR>', {})
map("n", "<leader>dso", '<cmd>lua require"dap".step_out()<CR>', {})
map("n", "<leader>dtb", '<cmd>lua require"dap".toggle_breakpoint()<CR>', {})
map("n", "<leader>dsc", '<cmd>lua require"dap.ui.variables".scopes()<CR>', {})
map("n", "<leader>dhh", '<cmd>lua require"dap.ui.variables".hover()<CR>', {})
map("v", "<leader>dhv", '<cmd>lua require"dap.ui.variables".visual_hover()<CR>', {})
map("n", "<leader>duh", '<cmd>lua require"dap.ui.widgets".hover()<CR>', {})
map("n", "<leader>duf", "<cmd>lua local widgets=require'dap.ui.widgets';widgets.centered_float(widgets.scopes)<CR>", {})
map("n", "<leader>dsbr", '<cmd>lua require"dap".set_breakpoint(vim.fn.input("Breakpoint condition: "))<CR>', {})
map("n", "<leader>dsbm", '<cmd>lua require"dap".set_breakpoint(nil, nil, vim.fn.input("Log point message: "))<CR>', {})
map("n", "<leader>dro", '<cmd>lua require"dap".repl.open()<CR>', {})
map("n", "<leader>drl", '<cmd>lua require"dap".repl.run_last()<CR>', {})
map("n", "<leader>dui", '<cmd>lua require"dapui".toggle()<CR>', {})

require("neodev").setup({
  library = { plugins = { "nvim-dap-ui" }, types = true },
})
require("dap-go").setup()
-- require('dap-python').setup('~/.virtualenv/debugpy/bin/python')
require('dap-python').setup('~/.virtualenv/smsguard_backend/bin/python')
require("nvim-dap-virtual-text").setup()

-- dap.adapters.chrome = {
--   type = "executable",
--   command = "node",
--   args = { os.getenv("HOME") .. "/vscode-chrome-debug/out/src/chromeDebug.js" },
-- }
--
-- dap.configurations.javascript = { -- change this to javascript if needed
--   {
--     type = "chrome",
--     request = "attach",
--     program = "${file}",
--     cwd = vim.fn.getcwd(),
--     sourceMaps = true,
--     protocol = "inspector",
--     port = 9222,
--     webRoot = "${workspaceFolder}",
--   },
-- }
--
-- dap.configurations.typescript = { -- change to typescript if needed
--   {
--     type = "chrome",
--     request = "attach",
--     program = "${file}",
--     cwd = vim.fn.getcwd(),
--     sourceMaps = true,
--     protocol = "inspector",
--     port = 9222,
--     webRoot = "${workspaceFolder}",
--   },
-- }

-- dap.adapters.dart = {
--   type = "executable",
--   command = "node",
--   args = { vim.fn.stdpath("data") .. "/dapinstall/dart_dbg/Dart-Code/out/dist/debug.js", "flutter"}
-- }

-- dap.configurations.dart = {
--   {
--     type = "dart",
--     request = "launch",
--     name = "Launch flutter",
--     dartSdkPath = os.getenv('HOME').."/flutter/bin/cache/dart-sdk/",
--     flutterSdkPath = os.getenv('HOME').."/flutter",
--     program = "${workspaceFolder}/lib/main.dart",
--     cwd = "${workspaceFolder}",
--   }
-- }

require("dapui").setup(
--   {
--   icons = { expanded = "▾", collapsed = "▸" },
--   mappings = {
--     -- Use a table to apply multiple mappings
--     expand = { "<CR>", "<2-LeftMouse>" },
--     open = "l",
--     remove = "d",
--     edit = "w",
--     repl = "r",
--   },
--   sidebar = {
--     -- You can change the order of elements in the sidebar
--     elements = {
--       -- Provide as ID strings or tables with "id" and "size" keys
--       {
--         id = "scopes",
--         size = 0.25, -- Can be float or integer > 1
--       },
--       { id = "breakpoints", size = 0.25 },
--       { id = "stacks", size = 0.25 },
--       { id = "watches", size = 00.25 },
--     },
--     size = 40,
--     position = "left", -- Can be "left", "right", "top", "bottom"
--   },
--   tray = {
--     elements = { "repl" },
--     size = 10,
--     position = "bottom", -- Can be "left", "right", "top", "bottom"
--   },
--   floating = {
--     max_height = nil, -- These can be integers or a float between 0 and 1.
--     max_width = nil, -- Floats will be treated as percentage of your screen.
--     mappings = {
--       close = { "q", "<Esc>" },
--     },
--   },
--   windows = { indent = 1 },
-- }
)

dap.listeners.after.event_initialized["dapui_config"] = function()
  dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
  dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
  dapui.close()
end
