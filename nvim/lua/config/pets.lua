require("pets").setup({
  row = 5,
  col = 0,
  speed_multiplier = 1,
  default_pet = "dog",
  default_style = "black",
  death_animation = true,
  random = false,
  popup = {
    width = "100%",
    winblend = 100,
    hl = { Normal = "Normal" },
    avoid_statusline = false,
  },
})
