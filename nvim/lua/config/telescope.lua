local map = vim.api.nvim_set_keymap
local trouble = require("trouble.providers.telescope")
local telescope = require("telescope")

telescope.setup {
  defaults = {
    mappings = {
      i = { ["<c-t>"] = trouble.open_with_trouble },
      n = { ["<c-t>"] = trouble.open_with_trouble },
    },
  },
}


require("telescope").load_extension("flutter")
require('telescope').load_extension('dap')

-- require("telescope").setup {
--   extensions = {
--     file_browser = {
--       theme = "ivy",
--       mappings = {
--         ["i"] = {
--           -- your custom insert mode mappings
--         },
--         ["n"] = {
--           -- your custom normal mode mappings
--         },
--       },
--     },
--   },
-- }

require("telescope").load_extension "file_browser"
-- require('telescope').load_extension('projects')

local options = { noremap = true }
map('n', '<leader>fP', '<cmd>Telescope projects<cr>', options)
map('n', '<leader>fp', '<cmd>Telescope find_files<cr>', options)
map('n', '<leader>ff', '<cmd>Telescope file_browser<cr>', options)
map('n', '<leader>fg', '<cmd>Telescope live_grep<cr>', options)
map('n', '<leader>fb', '<cmd>Telescope buffers<cr>', options)
map('n', '<leader>fm', '<cmd>Telescope marks<cr>', options)
map('n', '<leader>fh', '<cmd>Telescope oldfiles<cr>', options)

-- flutter
map('n', '<leader>fc', '<cmd>Telescope flutter commands<cr>', options)

-- dap
map('n', '<leader>fdc', '<cmd>Telescope dap commands<cr>', options)
map('n', '<leader>fdn', '<cmd>Telescope dap configurations<cr>', options)
map('n', '<leader>fdb', '<cmd>Telescope dap list_breakpoints<cr>', options)
map('n', '<leader>fdv', '<cmd>Telescope dap variables<cr>', options)
map('n', '<leader>fdf', '<cmd>Telescope dap frames<cr>', options)
