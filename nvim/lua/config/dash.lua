local map = vim.api.nvim_set_keymap
local fn = vim.fn
local cmd = vim.cmd
local plugins_count = fn.len(fn.globpath("~/.local/share/nvim/site/pack/packer/start", "*", 0, 1))
local db = require("dashboard")

local options = { noremap = true, silent = true }

db.setup({
  theme = "doom",
  config = {
    header = {
      "",
      "",
      "",
      "",
      "",
      " ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗",
      " ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║",
      " ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║",
      " ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║",
      " ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║",
      " ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝",
      "",
      "",
      "",
      "",
      "",
    }, --your header
    center = {
      {
        icon = "  ",
        desc = "Find File",
        -- keymap = "SPC f p",
        -- key = "p",
        action = "Telescope find_files",
      },
      {
        icon = "  ",
        desc = "Recents",
        -- keymap = "SPC f h",
        -- key = "p",
        action = "Telescope oldfiles",
      },
      {
        icon = "  ",
        desc = "Find Word",
        -- keymap = "SPC f g",
        -- key = "p",
        action = "Telescope live_grep",
      },
      {
        icon = "  ",
        desc = "Bookmarks",
        -- keymap = "SPC f m",
        -- key = "m",
        action = "Telescope marks",
      },
    },
    footer = {
      "",
      " Loaded " .. plugins_count .. " plugins",
      "",
      "   Neovim v0.8.2  ",
    }, --your footer
  },
})

db.default_executive = "telescope"
-- cmd("autocmd FileType dashboard set showtabline=0 | autocmd WinLeave <buffer> set showtabline=2")
cmd("autocmd FileType dashboard hi NonText guifg = bg | autocmd WinLeave <buffer> hi NonText guifg=#504945")

map("n", "<leader>ss", ":<C-u>SessionSave<CR>", {})
map("n", "<leader>sl", ":<C-u>SessionLoad<CR>", {})

-- map('n', '<leader>dh', ':DashboardFindHistory<cr>', options)
-- map('n', '<leader>df', ':DashboardFindFile<cr>', options)
-- map('n', '<leader>dg', ':DashboardFindWord<cr>', options)
-- map('n', '<leader>fm', ':DashboardJumpMark<cr>', options)
map("n", "<leader>fn", ":DashboardNewFile<cr>", options)

cmd([[au VimEnter * highlight dashboardFooter guifg=#b8bb26]])
cmd([[au VimEnter * highlight dashboardHeader guifg=#98971a]])
cmd([[au VimEnter * highlight dashboardCenter guifg=#fbf1c7]])
-- cmd[[au VimEnter * highlight dashboardkeymap guifg=#fabd2f]]
cmd([[au VimEnter * highlight dashboardkeymap guifg=#cc241d]])
