local o = vim.o
local g = vim.g
local cmd = vim.cmd

o.termguicolors = true
o.background = 'dark'
-- o.termguicolors = true

-- g.gruvbox_italic_keyword = true
-- g.gruvbox_italic_function = true

cmd 'colorscheme gruvbox'
