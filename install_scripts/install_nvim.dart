#! /usr/bin/env dcli

import 'package:dcli/dcli.dart';

void main() {
  print('Starting ...');

  if (!exists('$HOME/.config/nvim/undodir')) {
    createDir("$HOME/.config/nvim/undodir", recursive: true);
  }

  'ln -sf $HOME/Projects/dotfiles/init.vim $HOME/.config/nvim/init.vim'.run;
  'ln -sf $HOME/Projects/dotfiles/coc-settings.json $HOME/.config/nvim/coc-settings.json'
      .run;
  'ln -sf $HOME/Projects/dotfiles/.tmux.conf $HOME/.tmux.conf'.run;
  'tmux source-file $HOME/.tmux.conf'.run;

  print('Done');
}
